package com.combino.exceptions;

import java.io.IOException;

public class Tester {
    public static void main(String[] args) throws IOException {

        ExampleExceptions exceptions = new ExampleExceptions();
        exceptions.printCompileTimeException();
        exceptions.printUnCheckedException();
    }
}
