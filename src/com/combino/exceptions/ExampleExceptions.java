package com.combino.exceptions;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class ExampleExceptions {

    public void printCompileTimeException() throws IOException {

        File file = new File("C:/Users/VIHCADE/Desktop/sampleFile.txt");
        BufferedReader bufferedReader = new BufferedReader(new FileReader(file));
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            System.out.println(line);
        }

    }

    public void printUnCheckedException() {
        try {
            int ids[] = {1001, 1002, 1003, 1004, 1005, 1006, 1007, 1008, 1009};
            System.out.println(ids[4]);
            System.out.println(ids[9]);
        } catch (ArrayIndexOutOfBoundsException exception) {
            System.out.println("Something went wrong with printUncheckedException method....:)");
        } catch (IndexOutOfBoundsException exception) {
            System.out.println("Something went wrong with printUncheckedException method....:)");
        } catch (RuntimeException exception) {
            System.out.println("Something went wrong with printUncheckedException method....:)");
        } catch (Exception exception) {
            System.out.println("Something went wrong with printUncheckedException method....:)");
        } finally {
            System.out.println("This finally block that will get executed for sure");
        }

    }
}
