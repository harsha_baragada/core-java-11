package com.combino.multithreading;

public class Tester {
    public static void main(String[] args) {
       /* ExampleMultithreading thread1 = new ExampleMultithreading("thread-1");
        thread1.start();
        ExampleMultithreading thread2 = new ExampleMultithreading("thread-2");
        thread2.start();*/

        ExampleSynchronization exampleSynchronization = new ExampleSynchronization();
        ThreadDemo thread1 = new ThreadDemo("thread-1", exampleSynchronization);
        ThreadDemo thread2 = new ThreadDemo("thread-2", exampleSynchronization);
        thread1.start();
        thread2.start();
        try {

            thread1.join();
            thread2.join();
        } catch (Exception e) {
            System.out.println("the process has been interrupted");
        }
    }
}
