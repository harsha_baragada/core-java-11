package com.combino.multithreading;

public class ExampleDeadlock {

    public static Object lock1 = new Object();
    public static Object lock2 = new Object();

    public static void main(String[] args) {
        ThreadDemo1 threadDemo1 = new ThreadDemo1();
        ThreadDemo2 threadDemo2 = new ThreadDemo2();

        threadDemo1.start();
        threadDemo2.start();

    }

    private static class ThreadDemo1 extends Thread {

        public void run() {
            synchronized (lock1) {
                System.out.println("Thread-1 is holding lock 1");
                try {
                    Thread.sleep(10);
                } catch (Exception e) {
                    System.out.println("Thread 1 has been interrupted");
                }
                System.out.println("Thread-1 is waiting for thr lock 2");
                synchronized (lock2) {
                    System.out.println("Thread-1 is holding lock 1 & lock 2");

                }
            }
        }


    }

    private static class ThreadDemo2 extends Thread {

        public void run() {
            synchronized (lock1) {
                System.out.println("Thread-2 is holding lock 2");
                try {
                    Thread.sleep(10);
                } catch (Exception e) {
                    System.out.println("Thread 2 has been interrupted");
                }
                System.out.println("Thread-2 is waiting for thr lock 1");
                synchronized (lock2) {
                    System.out.println("Thread-2 is holding lock 1 & lock 2");

                }
            }
        }


    }
}
