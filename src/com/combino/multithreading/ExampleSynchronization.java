package com.combino.multithreading;

public class ExampleSynchronization {


    /*
     * synchronized (objectidentifier ){
     * // access shared resources inside
     * }
     * */


    public void printSomething() {

        try {
            for (int i = 0; i < 5; i++) {
                System.out.println("Count is ----" + i);
            }
        } catch (Exception e) {
            System.out.println("Oops..!! Something went wrong!");
        }
    }
}

class ThreadDemo extends Thread{
    private Thread thread;
    private String threadName;
    ExampleSynchronization exampleSynchronization;


    public ThreadDemo(String threadName, ExampleSynchronization exampleSynchronization){
        this.threadName = threadName;
        this.exampleSynchronization = exampleSynchronization;
    }



    public void run(){

        synchronized (exampleSynchronization) {
            exampleSynchronization.printSomething();
        }
        System.out.println("Thread "+threadName+" is existing");
    }

    public void start(){
        System.out.println("Starting "+threadName);
        if(thread== null){
            thread = new Thread(this, threadName);
            thread.start();
        }
    }

}
