package com.combino.multithreading;

public class ExampleMultithreading implements Runnable {

    private Thread thread;
    private String threadName;

    public ExampleMultithreading(String threadName) {
        this.threadName = threadName;
        System.out.println("Creating thread " + threadName);
    }

    @Override
    public void run() {

        System.out.println("The thread " + threadName + " is running");
        try {
            for (int i = 0; i < 5; i++
            ) {
                System.out.println("thread " + threadName + " " + i);
                Thread.sleep(50);
            }

        } catch (InterruptedException e) {
            System.out.println("Something went wrong with the thread ..!!");
        }
        System.out.println("Existing thread " + threadName);
    }


    public void start() {

        System.out.println("Starting thread " + threadName);
        if (thread == null) {
            thread = new Thread(this, threadName);
            thread.start();
        }
    }
}
