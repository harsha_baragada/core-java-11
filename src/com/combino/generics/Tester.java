package com.combino.generics;

import com.combino.collections.list.Person;

public class Tester {
    public static void main(String[] args) {


        String[] fruits = {"Apple","Mango", "Papaya", "Water melon"};
        Integer[] numbers = {1001,1108,6955,7899,9999,3214,5689};
        ExampleGenericMethods genericMethods = new ExampleGenericMethods();
        genericMethods.arrayIterator(fruits);
        genericMethods.arrayIterator(numbers);

        /*
        * Example generic class
        * */

        System.out.println("################ExampleGeneric class###############");
        /**
         * When we create object with person 'T' will become person
         * */
        ExampleGenericClass<Person> personExampleGenericClass = new ExampleGenericClass<>();
        personExampleGenericClass.setId(1001);
        personExampleGenericClass.setName("Peter");
        Person person = new Person();
        person.setName("Peter");
        person.setEmailId("Peter@parks.com");
        person.setMobileNumber(2589631473L);

        personExampleGenericClass.setT(person);

        System.out.println(personExampleGenericClass);
        /**
         * When we create object with person 'T' will become String
         * */
        ExampleGenericClass<String> exampleGenericClass = new ExampleGenericClass<>();

        exampleGenericClass.setId(1002);
        exampleGenericClass.setName("Eva");
        exampleGenericClass.setT("This is example for string");
        System.out.println(exampleGenericClass);
    }
}
