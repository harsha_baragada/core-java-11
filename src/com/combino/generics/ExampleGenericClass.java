package com.combino.generics;

public class ExampleGenericClass<T>  {


    private T t;

    private int id;
    private String name;

    public T getT() {
        return t;
    }

    public void setT(T t) {
        this.t = t;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "ExampleGenericClass{" +
                "t=" + t +
                ", id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
