package com.combino.generics;

public class ExampleGenericMethods {

    public <E> void arrayIterator(E[] elements){

        for (E  element: elements) {
            System.out.println(element);
        }
    }
}
