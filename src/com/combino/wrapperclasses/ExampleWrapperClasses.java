package com.combino.wrapperclasses;

import java.util.ArrayList;

public class ExampleWrapperClasses {
    Number number = 10;

    private int i = 10; // this is primitive data type
    private Integer x = 50; // creating property by using wrapper class

    private float p = 15.0f;
    private Float q = 50.5f;

    private Double xyz = 55.525;
    private double abc= 5589.36;

    private Character character= 'e'; // wrapper class
    private char aChar = 'A'; // primitive data type

    ArrayList<Integer> ints = new ArrayList<Integer>();

    public void printPrimitiveDataTypes() {

        System.out.println(number.floatValue());
        System.out.println("the hash code of xyz is "+xyz.hashCode());
    }
}
