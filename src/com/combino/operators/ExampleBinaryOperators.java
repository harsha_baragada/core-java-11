package com.combino.operators;

public class ExampleBinaryOperators {

    int a = 60, b = 13;

    public void binaryAnd() {
        System.out.println(a & b);
    }

    public void binaryOr() {
        System.out.println(a | b);
    }

    public void binaryXor() {
        System.out.println(a ^ b);
    }

    public void binaryCompliment() {
        System.out.println(~a);
    }

    public void binaryLeftShift() {
        System.out.println(a << 2);
    }

    public void binaryRightShift() {
        System.out.println(a >> 2);
    }

}
