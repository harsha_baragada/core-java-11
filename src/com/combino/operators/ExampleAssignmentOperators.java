package com.combino.operators;

public class ExampleAssignmentOperators {

    int i = 10;
    int x = 12, y = 50;
    int p = 11;

    public void printAssignments() {
        x += i; // x = x+ i
        y -= i; // y= y-i
        y *= x; // y = y*x;

        System.out.println(i);
        System.out.println(x);
        System.out.println(y);
    }



    public void exampleTerenaryOperator(){
        int s= (p==10)?20:5; // if the value of p is 11 then s will become 20 otherwise s will be 0
        System.out.println(s);
    }
}
