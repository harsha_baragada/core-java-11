package com.combino.operators;

public class ExampleLogicalOperator {


    public boolean logicalAnd(boolean a, boolean b) {
        return a && b;
    }

    public boolean logicalOr(boolean a, boolean b) {
        return a || b;
    }


    public boolean logicalNot(boolean a) {
        return !a;
    }
}
