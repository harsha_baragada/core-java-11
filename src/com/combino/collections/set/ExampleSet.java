package com.combino.collections.set;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

public class ExampleSet {
    Set<Integer> integerSet = new HashSet<>();

    public void printTheMethodOfSet(){
        integerSet.add(1111);
        integerSet.add(2222);
        integerSet.add(1111);
        integerSet.add(4444);
        integerSet.add(1111);
        integerSet.add(6666);
        integerSet.add(1111);
        integerSet.add(9999);

        System.out.println(integerSet);
        Iterator iterator = integerSet.iterator();
        while(iterator.hasNext())
            System.out.println( iterator.next());



    }

}
