package com.combino.collections.set;

import java.util.LinkedHashSet;


/*
* to maintain the insertion order since it's having linked list inside
* to eliminate the duplicate elements
* */
public class ExampleLinkedHashSet {

    LinkedHashSet<Integer> linkedHashSet = new LinkedHashSet<>();

    public void printLinkedHashSet() {

        linkedHashSet.add(6568);
        linkedHashSet.add(9999);
        linkedHashSet.add(7852);
        linkedHashSet.add(3214);
        linkedHashSet.add(6568);
        linkedHashSet.add(9747);
        linkedHashSet.add(6568);
        linkedHashSet.add(69875);
        linkedHashSet.add(6568);
        System.out.println(linkedHashSet);

    }
}
