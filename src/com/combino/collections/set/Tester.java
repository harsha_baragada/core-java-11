package com.combino.collections.set;

public class Tester {
    public static void main(String[] args) {
        ExampleSet set = new ExampleSet();
        set.printTheMethodOfSet();

        System.out.println("*************linked hashset******************");
        ExampleLinkedHashSet exampleLinkedHashSet = new ExampleLinkedHashSet();
        exampleLinkedHashSet.printLinkedHashSet();

        System.out.println("^^^^^^^^^^^^^^^^^^^Sorted set^^^^^^^^^^^^^^^^");
        ExampleSortedSet sortedSet = new ExampleSortedSet();
        sortedSet.printTheSortedSet();

    }
}
