package com.combino.collections.set;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.TreeSet;

public class ExampleSortedSet {

    TreeSet<Integer> sortedSet = new TreeSet<>();

    public void printTheSortedSet() {

        sortedSet.add(6548);
        sortedSet.add(8979);
        sortedSet.add(5468);
        sortedSet.add(2156);
        sortedSet.add(6548);
        sortedSet.add(8755);
        sortedSet.add(6548);
        sortedSet.add(1003);

        System.out.println(sortedSet);
        System.out.println(sortedSet.subSet(2156, 8755));
        System.out.println(sortedSet.headSet(6548));
        System.out.println(sortedSet.tailSet(6548));
        System.out.println(sortedSet.first());
        System.out.println(sortedSet.last());

        Iterator iterator = sortedSet.descendingIterator();

        while (iterator.hasNext()) {
            System.out.println(iterator.next());
        }

        Collection<Integer> collection = new ArrayList();

        collection.add(1522);
        collection.add(8686);
        collection.add(1522);
        collection.add(1522);
        collection.add(896898);
        collection.add(152682);
        collection.add(1522);
        collection.add(565);
        collection.add(1522);
        collection.add(8686);
        collection.add(1522);
        collection.add(1522);
        collection.add(896898);
        collection.add(152682);
        collection.add(1522);
        collection.add(565);

        System.out.println(collection);
        TreeSet<Integer> treeSet = new TreeSet<>(collection);
        System.out.println(treeSet);

    }
}
