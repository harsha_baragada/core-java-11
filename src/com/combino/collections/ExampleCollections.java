package com.combino.collections;

import java.util.*;


/*
*  Collections
*   --List
*       --* ArrayList -dynamic array but not thread safe
*       --* Vector -dynamic array and thread safe
*       --* LinkedList -follows linked list of entries (maintain insertion order)
*   --Set - set will not allow the duplicate elements
*       --* HashSet -it will follow hashing technique to store the elements so retrieving and searching is more easy
*       --* LinkedSet - maintain the linked list of entries by preventing duplicates
*       --* SortedSet(Tree set) -maintain ascending order by preventing duplicates
*   --Queue --  FIFO
*       --*PriorityQueue  -follows priority and natural ordering while storing the elements
*
*
*
*
*
* */
public class ExampleCollections {

    Collection<Integer> collection = new ArrayList<>();

    AbstractCollection<Integer> integerAbstractCollection = new ArrayList<>();

    public void printCollection() {

        System.out.println(collection.isEmpty() + " collection is empty");
        System.out.println(collection.add(1001));
        System.out.println(collection.add(1002));
        System.out.println(collection.add(1003));
        System.out.println(collection.add(1004));
        System.out.println(collection);
        System.out.println(collection.size());
        System.out.println(collection.isEmpty());
        System.out.println(collection.contains(1004) + " collection contains 1004");
        System.out.println(collection.contains(9999) + " collection contains 9999");
        Iterator iterator = collection.iterator();

        while (iterator.hasNext()){
            System.out.println(iterator.next());
        }

        System.out.println(collection.remove(1004));
        System.out.println(collection);
        System.out.println(collection.remove(1004));

        Collection<Integer> secondCollection = new ArrayList<>();
        secondCollection.add(9999);
        secondCollection.add(9001);
        secondCollection.add(9009);
        secondCollection.add(9004);
        collection.addAll(secondCollection);
        System.out.println(collection);
        System.out.println(collection.retainAll(secondCollection));
        System.out.println(collection);
        System.out.println(collection.hashCode());


    }
}
