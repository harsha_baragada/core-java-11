package com.combino.collections.list;

import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

public class ExampleList {

    List<String> friendsNames = new ArrayList<>();

    List<Person> friends = new ArrayList<>();

    Vector<Boolean> vector = new Vector();

    public void printArrayList() {

        friendsNames.add("Peter");
        friendsNames.add("Bob");
        friendsNames.add("Eva");
        friendsNames.add("Mark");
        friendsNames.add("Saurav");
        friendsNames.add("Rahul");
        friendsNames.add("Meena");

        System.out.println(friendsNames);


        Person peter = new Person();
        peter.setName("Peter");
        peter.setMobileNumber(96325874114L);
        peter.setEmailId("peter@petemail.com");
        peter.setAddress("Down town");
        friends.add(peter);

        Person rahul = new Person("Rahul",1235469787L,"rahul@gmail.com","Mid town");
        friends.add(rahul);


        System.out.println(friends);

        System.out.println(friendsNames.indexOf("Saurav"));
    }


}
