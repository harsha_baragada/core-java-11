package com.combino.collections.list;

import java.util.LinkedList;
import java.util.List;

public class ExampleLinkedList {

    LinkedList<Integer> ids = new LinkedList<>();
    LinkedList<List<String>> lists = new LinkedList<>();

    public void  printLinkedList(){


        ids.add(5345);
        ids.add(6876);
        ids.add(9564);
        ids.add(3677);
        ids.add(1265);
        ids.add(5875);
        ids.add(6546);
        ids.add(1564);
        ids.add(6987);

        System.out.println(ids);

        System.out.println(ids.getFirst());
        System.out.println(ids.getLast());
        System.out.println(ids.removeFirst());
        System.out.println(ids.removeLast());
        System.out.println(ids);
        ids.addFirst(9999);
        ids.addLast(11111);
        System.out.println(ids);
        System.out.println(ids.set(3,7777));
        System.out.println(ids);
        System.out.println(ids.poll());
        System.out.println(ids);
        System.out.println(ids.offer(4444));
        System.out.println(ids);
        System.out.println(ids.add(7777));
        System.out.println(ids);
        System.out.println(ids.removeFirstOccurrence(7777));
        System.out.println(ids);

    }
}
