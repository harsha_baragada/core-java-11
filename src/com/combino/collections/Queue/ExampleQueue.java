package com.combino.collections.Queue;

import java.util.PriorityQueue;
import java.util.Queue;

public class ExampleQueue {

    PriorityQueue<String> queue = new PriorityQueue<>();


    public void printQueue(){

        queue.add("peter");
        queue.add("emma");
        queue.add("marks");
        queue.add("heijn");
        queue.add("parker");
        queue.add("leva");

        System.out.println(queue);

        System.out.println(queue.peek());
        System.out.println(queue);
        System.out.println(queue.poll());
        System.out.println(queue);
        System.out.println(queue.remove("leva"));
        System.out.println(queue);
        System.out.println(queue.hashCode());
    }
}
