package com.combino.map;

import java.util.*;

public class ExampleSortedMap {

    public void exploreSortedMap(){

        Map<Integer,String> map = new HashMap<>();

        map.put(4444,"Cheetah");
        map.put(4044, "Bear");
        map.put(4589, "Oranguttan");
        System.out.println(map);
        SortedMap<Integer,String> sortedMap = new TreeMap(map);

        sortedMap.put(7623,"Dog");
        sortedMap.put(6637,"Elephant");
        sortedMap.put(9999,"Deer");
        sortedMap.put(6237,"Monkey");
        sortedMap.put(9099,"Horse");
        sortedMap.put(3342,"Tiger");
        sortedMap.put(7777,"Lion");

        System.out.println(sortedMap);

        System.out.println(sortedMap.subMap(3342,7777));
         /*Collections.reverseOrder(sortedMap.comparator());
        System.out.println(sortedMap);*/

        System.out.println(sortedMap.headMap(7777));
        System.out.println(sortedMap.tailMap(7777));
        System.out.println(sortedMap.lastKey());


        WeakHashMap hashMap = new WeakHashMap();
    }
}
