package com.combino.map;

public class Tester {
    public static void main(String[] args) {
        ExampleMap map = new ExampleMap();
        map.printMap();
        System.out.println("((((((((((((((((((((Hash map)))))))))))))))))))");
        ExampleHashMap hashMap = new ExampleHashMap();
        hashMap.printHashMap();
        System.out.println(")))))))))))))))))))) LinkedHashMap ((((((((((((((((");
        ExampleLinkedHashMap linkedHashMap = new ExampleLinkedHashMap();
        linkedHashMap.printLinkedHashMap();

        System.out.println(">>>>>>>>>>>>>>>>>>Sorted Map<<<<<<<<<<<<<<<<<<<<<<<");
        ExampleSortedMap sortedMap = new ExampleSortedMap();
        sortedMap.exploreSortedMap();
    }
}
