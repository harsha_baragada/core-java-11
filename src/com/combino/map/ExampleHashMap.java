package com.combino.map;

import java.util.HashMap;

public class ExampleHashMap {

    HashMap<String, String> names = new HashMap<>();

    public void printHashMap() {
        names.put("Peter","Parker");
        names.put("Eva","lessnen");
        names.put("Taco","Witte");
        names.put("Emile","Teson");
        names.put("Rahul","dev");

        System.out.println(names);

        System.out.println("Peter "+names.get("Peter"));

    }
}
