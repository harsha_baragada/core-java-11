package com.combino.map;

import java.util.LinkedHashMap;

public class ExampleLinkedHashMap {

    LinkedHashMap<Integer, String> linkedHashMap = new LinkedHashMap(16,0.75f,false);

    public void printLinkedHashMap() {

        linkedHashMap.put(3333, "Mango");
        linkedHashMap.put(2222, "Banana");
        linkedHashMap.put(5555, "Strawberry");
        linkedHashMap.put(1111, "Apple");
        linkedHashMap.put(4444, "Papaya");
        System.out.println(linkedHashMap);

        System.out.println(linkedHashMap.get(1111));
        System.out.println(linkedHashMap.get(4444));
        System.out.println(linkedHashMap.get(5555));
        System.out.println(linkedHashMap);

    }
}
