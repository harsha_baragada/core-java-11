package com.combino.map;

import java.util.HashMap;
import java.util.Hashtable;
import java.util.Map;

/**
 * Map<String, String> stringHashMap = new HashMap<>();
 * <p>
 * Map<Object, Object> objectObjectHashMap = new HashMap<>();
 */
public class ExampleMap {
    /*
     * maps will not allow the duplicate keys, but it will allow the duplicate values
     * */
    Map<Integer, String> map = new HashMap<>();

    Hashtable hashtable =  new Hashtable();
    public void printMap() {

        map.put(1001, "Hyderabad");
        map.put(1002, "Mumbai");
        map.put(1004, "Delhi");
        map.put(1003, "Chennai");
        map.put(1005, "Kolkata");

        map.put(9999, "Mumbai");
        map.put(1004, "Ahmedabad");
        map.put(7777, "Chennai");
        map.put(1005, "Ambala");


        System.out.println(map.put(1006, "Bengaluru"));

        System.out.println(map);
        System.out.println(map.size());
        System.out.println(map.isEmpty());
        System.out.println(map.containsKey(9999));
        System.out.println(map.containsKey(1003));
        System.out.println(map.containsValue("Hyderabad"));


        System.out.println(map.get(9999));
        System.out.println(map.remove(1004));
        System.out.println(map);

        System.out.println(map.keySet());
        System.out.println(map.values());
        System.out.println(map.hashCode());
    }
}
