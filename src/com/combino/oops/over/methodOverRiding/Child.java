package com.combino.oops.over.methodOverRiding;

public class Child extends Parent{

    public void saySomeThing() {
        System.out.println("Hello from the child class");
    }
}
