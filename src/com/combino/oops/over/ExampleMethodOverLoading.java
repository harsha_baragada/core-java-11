package com.combino.oops.over;

public class ExampleMethodOverLoading {

    public double sum(int a, int b) {
        return a + b;
    }

    public int sum(int a, int b, int c) {
        return a + b + c;
    }

    public double sum(double a, double b) {
        return a + b;
    }

    // this kind of overloading is not possible in java
   /* public double sum(int a, int b) {
        return a + b;
    }*/
}
