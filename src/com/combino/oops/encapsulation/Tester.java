package com.combino.oops.encapsulation;

public class Tester {
    public static void main(String[] args) {
        Person person = new Person();

        person.setId(1001);
        person.setWeight(65.6);
        person.setAge(27.6f);
        person.setEmail("Bobmarley@marloes.com");
        person.setMobileNumber(2369851478L);
        person.setName("Bob");

        System.out.println(person);

        System.out.println("Hey this is " + person.getName() + " of age " + person.getAge() + " and I am weighing around " + person.getWeight() + " I am a singer. You can always reach me on my email: " + person.getEmail() + " and mobile:  " + person.getMobileNumber());

    }
}
