package com.combino.oops.abstraction;

public class ExampleAbstractClassImpl extends ExampleAbstractClass {
    @Override
    public void someAbstractMethod() {
        System.out.println("someAbstractMethod");
    }

    @Override
    public void someAbstractMethod1() {
        System.out.println("someAbstractMethod");

    }

    @Override
    public void someAbstractMethod2() {
        System.out.println("someAbstractMethod");

    }

    @Override
    public void someAbstractMethod3() {
        System.out.println("someAbstractMethod");

    }

    @Override
    public void someAbstractMethod4() {
        System.out.println("someAbstractMethod");

    }

    @Override
    public void someAbstractMethod5() {
        System.out.println("someAbstractMethod");

    }
}
