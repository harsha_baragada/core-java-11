package com.combino.oops.abstraction.example;

public class TemporaryEmployee extends Employee {
    private double fixedComponent = 9999;
    private double variableComponent = 125;

    @Override
    public double calculateSalary() {
        return fixedComponent + variableComponent;
    }
}
