package com.combino.oops.abstraction.example;

public class Tester {
    public static void main(String[] args) {

        Employee employee = new PermanentEmployee();
        System.out.println(employee.calculateSalary());

        Employee employee1 = new TemporaryEmployee();
        System.out.println(employee1.calculateSalary());
    }
}
