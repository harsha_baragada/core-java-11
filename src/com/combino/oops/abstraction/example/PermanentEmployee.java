package com.combino.oops.abstraction.example;

public class PermanentEmployee extends Employee {
    private double fixedComponent = 9963.0;
    private double variableComponent = 782;
    private double incentive = 9999;

    @Override
    public double calculateSalary() {
        return fixedComponent + variableComponent + incentive;
    }
}
