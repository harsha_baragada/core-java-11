package com.combino.oops.abstraction;


/*
 * When a class is abstract it may or may not contain abstract methods
 *
 * When a class contains abstract method then the class should marked as an abstract class
 * */
public abstract class ExampleAbstractClass {

    public abstract void someAbstractMethod();

    public abstract void someAbstractMethod1();

    public abstract void someAbstractMethod2();

    public abstract void someAbstractMethod3();

    public abstract void someAbstractMethod4();

    public abstract void someAbstractMethod5();

}
