package com.combino.oops.inheritance;

public class Employee {

    private int id;
    private String name;
    private String email;
    // employee has a department (has a relation)
    private Department department;
}
