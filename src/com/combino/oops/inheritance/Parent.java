package com.combino.oops.inheritance;

public class Parent {

    private int id;

    public Parent(int id) {
        this.id = id;
    }

    public Parent() {
    }

    public void thisIsAMethodFromParentClass() {
        System.out.println("hello this is a method from the parent class");
    }
}
