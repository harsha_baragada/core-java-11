package com.combino.oops.exampleInterface;

public class Tester {
    public static void main(String[] args) {
        ExampleInterface.exampleStaticMethod();
        System.out.println(ExampleInterface.NAME);

        ExampleInterface exampleInterface = new ExampleInterfaceImpl();
        InterFaceOne interFaceOne = new ExampleInterfaceImpl();
        InterfaceTwo interfaceTwo = new ExampleInterfaceImpl();
    }
}
