package com.combino.oops.exampleInterface;

public interface ExampleInterface extends InterFaceOne,InterfaceTwo{

    String NAME = "This is a constant from  the interface";

   void exampleMethod();

    static void exampleStaticMethod() {
        System.out.println("This is a static method from the interface");
    }

}
