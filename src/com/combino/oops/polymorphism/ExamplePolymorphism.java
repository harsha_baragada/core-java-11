package com.combino.oops.polymorphism;


/*
 * The ability of an object to take on multiple forms is called as polimorphism
 * */
public class ExamplePolymorphism {

    public static void main(String[] args) {

        Animal animal = new Dog(); // dog is an animal
        Mammal mammal = new Dog(); // dog is a mammal
        Dog dog = new Dog(); // dog is a dog
        Object o = new Dog(); // dog is an object

    }
}
