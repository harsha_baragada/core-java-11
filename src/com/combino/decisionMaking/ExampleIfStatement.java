package com.combino.decisionMaking;

public class ExampleIfStatement {

    public void makeADecision(int a) {

        if (a % 2 == 0) {
            System.out.println(a + " is exactly divided by 2");
            if (a % 3 == 0) {
                System.out.println(a + " is exactly divided by 3");
            } else {
                System.out.println(a + " is not exactly divided by 3");
            }
            if (a % 4 == 0) {
                System.out.println(a + " is exactly divided by 4");
            } else {
                System.out.println(a + " is not exactly divided by 4");
            }
        } else {
            System.out.println(a + " is not exactly divided by 2");
        }
    }
}
