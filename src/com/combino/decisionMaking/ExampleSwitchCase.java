package com.combino.decisionMaking;

public class ExampleSwitchCase {


    int y = 40, x;

    public void decideGrade(char grade) {

        switch (grade) {
            case 'A':
                System.out.println("Excellent");
                break;
            case 'B':
                System.out.println("Very Good");
                break;
            case 'C':
                System.out.println("Good");
                break;
            case 'D':
                System.out.println("Satisfactory");
                break;
            case 'E':
                System.out.println("Passed");
                break;
            case 'F':
                System.out.println("Fail");
                break;
            default:
                System.out.println("Given grade is not having any remarks");
        }

    }


    public void decideTheValueOfX() {
        x = (y > 20) ? 10 : 5;
        System.out.println(x);
    }
}
