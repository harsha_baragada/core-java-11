package com.combino.java8;

public class Tester {
    public static void main(String[] args) {
        ExampleLambdaExpressions exampleLambdaExpressions = new ExampleLambdaExpressions();
        exampleLambdaExpressions.printACollection();
        ExampleMethodReferences methodReferences = new ExampleMethodReferences();
        methodReferences.printNames();
        methodReferences.printFunctionalNumbers();
        System.out.println("streams");
        ExampleStreams streams = new ExampleStreams();
        streams.printStreams();
    }
}
