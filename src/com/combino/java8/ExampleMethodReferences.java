package com.combino.java8;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

/*
 *
 *   ::
 * */
public class ExampleMethodReferences {

    List<Integer> list = Arrays.asList(1, 566, 9009, 1001, 2002, 5225, 3003);

    public void printNames() {
        ArrayList<String> names = new ArrayList<>();
        names.add("Mahesh");
        names.add("Ramesh");
        names.add("Suresh");
        names.add("Rupesh");
        names.add("Kamalesh");

        names.forEach(System.out::println);


    }


    public void printFunctionalNumbers() {

        System.out.println("Print all numbers");

        System.out.println("print all numbers");
        eval(list, n -> true);

        System.out.println("print all even numbers");

        eval(list, n -> n % 2 == 0);

        System.out.println("print all numbers that are greater than 3");
        eval(list, n -> n > 3);

    }


    public void eval(List<Integer> list, Predicate<Integer> predicate) {
        for (Integer integer : list
        ) {
            if (predicate.test(integer))
            System.out.println(integer + " ");
        }
    }
}
