package com.combino.java8;


import java.util.ArrayList;
import java.util.Collection;

/**
 * parameter  -> body
 */

public class ExampleLambdaExpressions {

    Collection<String> cities = new ArrayList<>();

    public void printACollection() {

        cities.add("Hyderabad");
        cities.add("Mumbai");
        cities.add("Newyork");
        cities.add("Paris");
        cities.add("Cairo");

        cities.forEach(city ->
                System.out.println(city.toUpperCase())
        );

    }

}
