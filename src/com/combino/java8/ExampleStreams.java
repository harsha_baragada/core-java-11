package com.combino.java8;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class ExampleStreams {

    public void printStreams() {

        String[] names = {"Mango", "Apple", "", "Banana", "", "Papaya", "Melon"};
        List<String> fruits = Arrays.asList(names);
        System.out.println(fruits);
        List<String> filteredFruits = fruits.stream().filter(fruit -> !fruit.isEmpty()).collect(Collectors.toList());
        System.out.println(filteredFruits);
    }
}
