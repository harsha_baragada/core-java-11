package com.combino.datastructures;

import java.util.Dictionary;
import java.util.Enumeration;
import java.util.Hashtable;

public class ExampleDictionary {
    Dictionary<Integer, String> dictionary = new Hashtable<Integer, String>();

    public void printDictionary() {
        dictionary.put(1001, "bob");
        dictionary.put(1002, "Eva");
        dictionary.put(1003, "Peter");
        dictionary.put(1004, "Moly");
        System.out.println(dictionary);
        System.out.println(dictionary.size());

        Enumeration ids = dictionary.keys();
        while (ids.hasMoreElements()) {
            System.out.println(ids.nextElement());
        }

        System.out.println(System.getProperties());
    }
}
