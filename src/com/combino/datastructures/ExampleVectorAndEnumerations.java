package com.combino.datastructures;

import java.util.BitSet;
import java.util.Enumeration;
import java.util.Stack;
import java.util.Vector;

public class ExampleVectorAndEnumerations {

    Vector<String> cities = new Vector();
    Enumeration<String> cityEnumeration;
    BitSet bitSet = new BitSet();
    Stack<String> stack = new Stack<>();

    public void printVector() {
        cities.add("Mumbai");
        cities.add("Delhi");
        cities.add("Hyderbad");
        cities.add("Ahmedabad");
        cities.add("Kolkata");
        cities.add("Chennai");
        System.out.println(cities);
        System.out.println(cities.size());
        cities.remove("Ahmedabad");
        System.out.println(cities);
        System.out.println(cities.size());

        cityEnumeration = cities.elements();

        while (cityEnumeration.hasMoreElements()) {
            System.out.println(cityEnumeration.nextElement());
        }
    }


    public void printStack() {
        stack.add("Zero to one");
        stack.add("psychology of money");
        stack.add("Why do we need to start with why");
        stack.add("Design by change");

        System.out.println(stack);
        System.out.println(stack.pop());
        System.out.println(stack);
        System.out.println(stack.peek());
        System.out.println(stack);
    }
}
