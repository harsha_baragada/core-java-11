package com.combino.datastructures;

public class Tester {
    public static void main(String[] args) {
        ExampleVectorAndEnumerations vectorAndEnumerations = new ExampleVectorAndEnumerations();
        vectorAndEnumerations.printVector();
        vectorAndEnumerations.printStack();

        ExampleDictionary dictionary = new ExampleDictionary();
        dictionary.printDictionary();
    }

}
