package com.combino.strings;

public class ExampleStringBufferAndBuilder {

    StringBuffer stringBuffer = new StringBuffer("This is just a sample string buffer");
    StringBuilder stringBuilder = new StringBuilder("You must cross a course cross cow");

    public void printStringBuilderAndBuffer() {

        System.out.println(stringBuilder);
        System.out.println(stringBuilder.reverse());
        System.out.println(stringBuilder.substring(4));
        System.out.println(stringBuilder.delete(4,13));
        System.out.println(stringBuilder.reverse());
    }
}
