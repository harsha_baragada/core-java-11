package com.combino.strings;

import java.nio.charset.StandardCharsets;

public class ExampleString {

    private String name = "Perter parker";
    char[] fruit = {'A', 'P', 'P', 'L', 'E'};
    String fruitString = new String(fruit);
    String emptyString = " ";


    String city = "Newyork";
    String cty = "nEwYoRk";

    public void printString() {
        System.out.println(name);
        System.out.println(fruit);
        System.out.println(fruitString);
        System.out.println(fruitString.length());
        name = "xxxx";
        System.out.println(name);
        System.out.println(emptyString.isEmpty());
        System.out.println(emptyString.isBlank());
        System.out.println(fruitString.charAt(3));
        System.out.println(city.equals(cty));
        System.out.println(city.equalsIgnoreCase(cty));
        System.out.println(city.getBytes(StandardCharsets.UTF_8));
        System.out.println(cty.toLowerCase());
        System.out.println(cty.toUpperCase());
        System.out.println(cty.replace('n', 'p'));

    }


    public void concatinatingStrings() {

        String x = "Hello..";
        String y = " World !";


        System.out.println(x + y);
        System.out.println(x.concat(y));
        System.out.println(x.concat(" World !"));
        System.out.println("Hello..".concat(y));
        System.out.println("Hello..".concat(" World !"));
        System.out.println("Hello.."+" World !");
    }
}
