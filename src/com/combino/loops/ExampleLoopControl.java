package com.combino.loops;

public class ExampleLoopControl {

    public void printLoopControls() {

        for (int i = 0; i < 10; i++) {
            if (i == 6) {
                break;
            }
            System.out.println(i);
        }

    }

    public void printContinueStatement() {
        System.out.println();
        for (int i = 0; i < 10; i++) {
            if (i == 6) {
                continue;
            }
            System.out.println(i);
        }

    }
}
