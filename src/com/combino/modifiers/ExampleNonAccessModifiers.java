package com.combino.modifiers;


/*
 * static -- used to create methods and variables that will exist independently
 * final -- used top create only one copy of a variable or a method
 * */
public final class ExampleNonAccessModifiers {

    public final static int ID = 1001;
    private final String department = "ME";

    public  static void someStaticMethod() {

        System.out.println("This is a static method!!");
    }

}
